import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { GithubUser } from "./github-user";
import { GithubRepository } from "./github-repo";

@Injectable()
export class GithubService {
    private API: string = 'https://api.github.com';

    constructor(private httpClient: HttpClient) { }

    GetUser(username: string): Promise<GithubUser> {
        const url: string = `${this.API}/users/${username}`
        return new Promise((resolve, reject) => {
            if (!username || username.trim() === '') {
                reject('Username empty');
            }
            this.httpClient.get<GithubUser>(url)
                .subscribe(user => resolve(user), error => reject(error));
        });
    }

    GetRepos(username: string): Promise<GithubRepository[]> {
        const url: string = `${this.API}/users/${username}/repos`
        return new Promise((resolve, reject) => {
            if (!username || username.trim() === '') {
                reject('Username empty');
            }
            this.httpClient.get<GithubRepository[]>(url)
                .subscribe(repos => resolve(repos), error => reject(error));
        });
    }
}