import { Component } from '@angular/core';
import { GithubUser } from './domains/github/github-user';
import { GithubService } from './domains/github/github.service';
import { GithubRepository } from './domains/github/github-repo';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  username: string = 'thiagofmleite';
  user: GithubUser = null;
  repositories: GithubRepository[] = [];
  constructor(private service: GithubService, public snackBar: MatSnackBar) { }

  handleUsernameUpdated(event: string): any {
    if(event == null || event.trim() === '') {
      this.showAlert('Invalid username', null);
      return;
    }
    this.username = event;
    this.service.GetUser(this.username)
      .then(user => {
        this.user = user;
        return this.service.GetRepos(this.username)
      })
      .then(repos => {
        this.repositories = repos;
      })
      .catch(error => {
        this.showAlert('Your request could not be completed', null);
        console.error(error);
      })
  }

  showAlert(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
