import { Component, OnInit, Input } from '@angular/core';
import { GithubRepository } from '../../domains/github/github-repo';

@Component({
    selector: 'app-repository-item',
    templateUrl: './repository-item.component.html',
    styleUrls: ['./repository-item.component.scss']
})
export class RepositoryItemComponent implements OnInit {
    @Input() repository: GithubRepository = null;
    
    constructor() { }

    ngOnInit(): void { }
}
