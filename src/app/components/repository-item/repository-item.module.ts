import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatExpansionModule, MatButtonModule, MatTabsModule } from '@angular/material';
import { RepositoryItemComponent } from './repository-item.component';

@NgModule({
    declarations: [RepositoryItemComponent],
    imports: [ CommonModule, MatExpansionModule, MatButtonModule, MatTabsModule ],
    exports: [RepositoryItemComponent],
    providers: [],
})
export class RepositoryItemModule {}