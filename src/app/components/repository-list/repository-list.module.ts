import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatExpansionModule } from '@angular/material';
import { RepositoryListComponent } from './repository-list.component';
import { RepositoryItemModule } from '../repository-item/repository-item.module';

@NgModule({
    declarations: [RepositoryListComponent],
    imports: [CommonModule, MatExpansionModule, RepositoryItemModule],
    exports: [RepositoryListComponent],
    providers: [],
})
export class RepositoryListModule { }