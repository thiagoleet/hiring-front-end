import { Component, OnInit, Input } from '@angular/core';
import { GithubRepository } from '../../domains/github/github-repo';

@Component({
    selector: 'app-repository-list',
    templateUrl: './repository-list.component.html',
    styleUrls: ['./repository-list.component.scss']
})
export class RepositoryListComponent implements OnInit {
    @Input() repositories: GithubRepository[] = []

    constructor() { }

    ngOnInit(): void { }
}
