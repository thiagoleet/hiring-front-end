import { Component, OnInit, Input } from '@angular/core';
import { GithubUser } from '../../domains/github/github-user';

@Component({
    selector: 'app-user',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
    @Input() user: GithubUser = null;
    constructor() { }

    ngOnInit(): void { }
}
