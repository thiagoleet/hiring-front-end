import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatCardModule, MatButtonModule, MatIconModule } from '@angular/material';
import { UserComponent } from './user.component';

@NgModule({
    declarations: [UserComponent],
    imports: [CommonModule, MatCardModule, MatButtonModule, MatIconModule],
    exports: [UserComponent],
    providers: [],
})
export class UserModule { }