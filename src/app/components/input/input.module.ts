import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule, MatInputModule, MatButtonModule } from '@angular/material';
import { InputComponent } from './input.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [InputComponent],
    imports: [CommonModule, FormsModule, MatFormFieldModule, MatInputModule, MatButtonModule],
    exports: [InputComponent],
    providers: [],
})
export class InputModule { }