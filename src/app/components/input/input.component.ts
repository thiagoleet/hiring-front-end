
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
    selector: 'app-input',
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss']
})
export class InputComponent implements OnInit {
    @Input() username: string;
    @Output() usernameUpdated: EventEmitter<string> = new EventEmitter<string>();

    constructor() { }

    ngOnInit(): void { }
    submit(event = null) {
        if (event) {
            event.preventDefault();
        }
        this.usernameUpdated.emit(this.username);
    }
}
