import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TestBed, async, inject } from '@angular/core/testing';
import { GithubService } from "./domains/github/github.service";
import { GithubUser } from "./domains/github/github-user";

describe('GithubService', () => {
  let service: GithubService;
  let username: string = 'thiagofmleite';
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [HttpClient, GithubService]
    })

    service = TestBed.get(GithubService);
  });

  it('#GetUser login should match user input',
    (done: DoneFn) => {
      service.GetUser(username).then(value => {
        expect(value.login).toBe(username);
        done();
      });
    });

  it('#GetUser should return an object',
    (done: DoneFn) => {
      service.GetUser(username).then(value => {
        expect(typeof (value)).toBe('object');
        done();
      });
    });

  it('#GetUser should not accept empty username',
    (done: DoneFn) => {
      service.GetUser(null).catch(error => {
        expect(error).toBe('Username empty');
        done();
      });
    });
  
  it('#GetRepos should return an array (empty or filled)',
    (done: DoneFn) => {
      service.GetRepos(username).then(value => {
        expect(value.length).toBeGreaterThanOrEqual(0);
        done();
      });
    });


  it('#GetRepos should not accept empty username',
    (done: DoneFn) => {
      service.GetRepos(null).catch(error => {
        expect(error).toBe('Username empty');
        done();
      });
    });

});