import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MaterialModule } from './shared/material/material.module';
import { InputModule } from './components/input/input.module';
import { GithubService } from './domains/github/github.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { UserModule } from './components/user/user.module';
import { RepositoryListModule } from './components/repository-list/repository-list.module';
import { RepositoryItemModule } from './components/repository-item/repository-item.module';
import { MatSnackBarModule } from '@angular/material';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    InputModule,
    UserModule,
    RepositoryListModule,
    RepositoryItemModule
  ],
  providers: [GithubService, HttpClient],
  bootstrap: [AppComponent]
})
export class AppModule { }
